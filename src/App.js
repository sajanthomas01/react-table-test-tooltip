import React from 'react'
import styled from 'styled-components'
import { useTable } from 'react-table'

import makeData from './makeData'

const Styles = styled.div`
  padding: 1rem;

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
`

function Table({ columns, data }) {
  // Use the state and functions returned from useTable to build your UI
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({
      columns,
      data,
    })

  // Render the UI for your table
  return (
    <div style={{ width: '100%', marginTop: 50 }}>
      <div>
        <table
          {...getTableProps()}
          style={{
            overflowY: 'hidden',
            maxWidth: '75%',
            display: 'inline-block',
          }}
        >
          <thead>
            {headerGroups.map(headerGroup => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                  <th {...column.getHeaderProps()}>
                    <div className="headerContainer">
                      <span className="tooltip">Tooltip</span>
                      <div>{column.render('Header')}</div>
                    </div>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map(cell => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </div>
  )
}

function App() {
  const columns = React.useMemo(
    () => [
      {
        Header: 'Age',
        accessor: 'age',
      },
      {
        Header: 'Visits',
        accessor: 'visits',
      },
      {
        Header: 'Visits',
        accessor: 'visits',
      },
      {
        Header: 'Visits',
        accessor: 'visits',
      },
      {
        Header: 'Visits',
        accessor: 'visits',
      },
      {
        Header: 'Visits',
        accessor: 'visits',
      },
      {
        Header: 'Visits',
        accessor: 'visits',
      },
      {
        Header: 'Visits',
        accessor: 'visits',
      },
      {
        Header: 'Visits',
        accessor: 'visits',
      },
      {
        Header: 'Status',
        accessor: 'status',
      },
      {
        Header: 'Progress',
        accessor: 'progress',
      },
      {
        Header: 'Progress',
        accessor: 'progress',
      },
      {
        Header: 'Progress',
        accessor: 'progress',
      },
      {
        Header: 'Progress',
        accessor: 'progress',
      },
      {
        Header: 'Progress',
        accessor: 'progress',
      },
      {
        Header: 'Progress',
        accessor: 'progress',
      },
      {
        Header: 'Progress',
        accessor: 'progress',
      },
      {
        Header: 'Progress',
        accessor: 'progress',
      },
    ],

    []
  )

  const data = React.useMemo(() => makeData(20), [])

  return (
    <Styles>
      <Table columns={columns} data={data} />
    </Styles>
  )
}

export default App
